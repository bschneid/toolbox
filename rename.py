#!/usr/bin/env python2

""" Rename one TBranch in a TTree. """

from ToolboxTree import ToolboxTree

if __name__ == '__main__':
    MY_TOOLBOX = ToolboxTree()
    MY_TOOLBOX.name_rootfile_out = "output.root"
    MY_TOOLBOX.rename_branch("../data/singlelepton-trees/merged/histfitter_signal.root",
                             "T1tttt_mGo1200_mChi800_CENTRAL", "nJet", "nJets")
