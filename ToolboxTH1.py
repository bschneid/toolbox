#!/usr/bin/env python2

""" Toolbox classes for various operations on ROOT TH1 histograms. """

from os import makedirs, chdir, getcwd, path
from ROOT import TFile, TH1F, TCanvas, TLegend  # pylint: disable=import-error
from Logger import LGR

class ToolboxTH1(object):

    """ Toolbox class for various operations on ROOT TH1's. """

    def __init__(self):

        """ Initialize object variables. """

        self.rootfile = TFile()
        self.directory = ''
        self._clear()

    def _clear(self):

        """ Initializes/clears ROOT objects. """

        self._histogram = TH1F()
        self._canvas = TCanvas()
        self._legend = TLegend(0.9, 0.2, 1.00, 0.9)

    def create_histogram(self, name, title, coordinate_x):

        """ Create TH1 with sensible binning. """

        pass

    def modify_axes(self, axis_x, y_low=0., y_high=100.):

        """ Modify axes of histogram. """

        self._histogram.SetStats(False)
        self._histogram.GetXaxis().SetTitle(axis_x)
        self._histogram.GetZaxis().SetRangeUser(y_low, y_high)

    def _add_legend(self, histos):

        """ Add TLegend. """

        self._legend.SetBorderSize(0)
        #self._legend.SetTextSize(0.)
        for histo in histos:
            self._legend.AddEntry(histo, histo.GetTitle(), 'f')
        self._legend.Draw()

    def save(self):

        """ Save histogram in TFile and as *.pdf. """

        # Go into directory if it is defined
        if self.directory:
            cwd = getcwd()

            # Change directory in rootfile
            if not self.rootfile.GetDirectory(self.directory):
                self.rootfile.mkdir(self.directory)
            self.rootfile.cd(self.directory)

            # Change directory on filesystem
            if not path.exists(self.directory):
                makedirs(self.directory)
            chdir(self.directory)

        self._canvas.SaveAs('{}.pdf'.format(self._histogram.GetName()))
        self._histogram.Write()

        # Go back to original working directories
        if self.directory:
            self.rootfile.cd()
            chdir(cwd)

        self._clear()
