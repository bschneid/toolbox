#!/usr/bin/env python2

""" Toolbox classes for various operations on ROOT TH2 histograms. """

from os import makedirs, chdir, getcwd, path
from array import array
from collections import defaultdict
from ROOT import TFile, TH2F, TCanvas, TGraph  # pylint: disable=import-error
from ROOT import TLine, TLegend, TLatex  # pylint: disable=import-error
from Logger import LGR


class ToolboxTH2(object):  # pylint: disable=too-many-instance-attributes

    """ Toolbox class for various operations on ROOT TH2's. """

    def __init__(self):

        """ Initialize object variables. """

        self.rootfile = TFile()
        self.directory = ''
        self._clear()

    def _clear(self):

        """ Initializes/clears ROOT objects. """

        self._histogram = TH2F()
        self._dc_subhistos = []
        self._canvas = TCanvas()
        # Set margin on the right, so that axis labels are still on TCanvas
        # Default value is 0.1
        self._canvas.SetRightMargin(.14)
        self._star = TGraph()
        self._line = TLine()
        self._legend = TLegend(0.9, 0.2, 1.00, 0.9)
        self._text = []

    def create_histogram(self, name,  # pylint: disable=too-many-arguments
                         title, coordinate_x, coordinate_y, scale=1):

        """ Create TH2 with sensible binning. """

        # All lists must have same length > 0
        if len(coordinate_x) != len(coordinate_y) or len(coordinate_x) == 0:
            raise IndexError('The lists that you provided to '
                             'create_histogram() must have same length and '
                             'be longer than 0.')

        # Define binning from coordinates
        no_of_bins_x = scale*len(set(coordinate_x))
        no_of_bins_y = scale*len(set(coordinate_y))

        # The offset adds half a bin to the beginning and the end of the
        # histogram, to make the actual values appear centrally
        offset_x = self._get_offset(coordinate_x, no_of_bins_x, scale)
        offset_y = self._get_offset(coordinate_y, no_of_bins_y, scale)

        min_x = min(coordinate_x) - offset_x
        min_y = min(coordinate_y) - offset_y
        max_x = max(coordinate_x) + offset_x
        max_y = max(coordinate_y) + offset_y

        LGR.debug('Histogram created with %s bins in x from %s to %s and '
                  'offset %s.', no_of_bins_x, min_x, max_x, offset_x)
        LGR.debug('Histogram created with %s bins in y from %s to %s and '
                  'offset %s.', no_of_bins_y, min_y, max_y, offset_y)

        self._histogram = TH2F(name, title, no_of_bins_x, min_x, max_x,
                               no_of_bins_y, min_y, max_y)

    def _get_offset(self, coordinates, no_of_bins, scale):

        """ Return offset which is equal to the size of half a bin. It adds
        half a bin to the beginning and the end of the histogram, to make
        the actual values appear centrally. """

        return max(1, (max(coordinates) - min(coordinates)) \
                   / max(1, (2*no_of_bins/scale - 2)))

    def modify_axes(self, axis_x, axis_y, z_low=0., z_high=100.):

        """ Modify axes of histogram. """

        self._histogram.SetStats(False)
        self._histogram.GetXaxis().SetTitle(axis_x)
        self._histogram.GetYaxis().SetTitle(axis_y)
        self._histogram.GetZaxis().SetRangeUser(z_low, z_high)

    def plot_numbers(self, coordinate_x,  # pylint: disable=too-many-arguments
                     coordinate_y, coordinate_z, scale=1, decimals=1):

        """ Plot numbers in a TH2. Multiply z-values by scale and round them to
        n decimals. """

        # If z plotting range is outside axis range, redefine range
        if min(coordinate_z) < self._histogram.GetMinimum():
            self._histogram.SetMinimum(min(coordinate_z)-1.)
        if max(coordinate_z) > self._histogram.GetMaximum():
            self._histogram.SetMaximum(max(coordinate_z)-1.)

        LGR.debug('Fill histogram with x = %s, y = %s, z = %s.',
                  coordinate_x, coordinate_y, coordinate_z)
        # All lists must have same length
        if len(coordinate_x) != len(coordinate_y) or \
           len(coordinate_x) != len(coordinate_z):
            raise IndexError('The lists that you provided to plot_numbers() '
                             'do not have the same length.')

        # Loop over one list and fill histogram
        for idx in range(0, len(coordinate_x)):
            self._histogram.Fill(coordinate_x[idx], coordinate_y[idx],
                                 round(scale*coordinate_z[idx], decimals))
        self._histogram.Draw('COLZ|text')

    def plot_dcs(self, coordinate_x, coordinate_y, dcs):

        """ Plot decay channels in a TH2. """

        # Global coordinates for subhistos:
        # Lower edge, higher edge, number of bins, offset (half a bin)
        x_nb = self._histogram.GetXaxis().GetNbins()
        x_os = self._get_offset(coordinate_x, x_nb, 10)

        y_nb = self._histogram.GetYaxis().GetNbins()
        y_os = self._get_offset(coordinate_y, y_nb, 10)

        # Define list with unique process categories that have at least one
        # entry in the subhistos; this list is passed later to the legend
        subhistos_legend = []

        # Loop over one list and fill histogram
        for idx1 in range(0, len(coordinate_x)):

            c_x = coordinate_x[idx1]
            c_y = coordinate_y[idx1]
            dco = dcs[idx1]

            # Local coordinates for subhistos
            # Only use 90 % of the offset, to allow for a border between the
            # bins, to make them easier distinguishable
            x_lo_bin = c_x - .9*(x_os)
            x_hi_bin = c_x + .9*(x_os)
            y_lo_bin = c_y - .9*(y_os)
            y_hi_bin = c_y + .9*(y_os)

            LGR.debug('x: no bins: %s; offset: %s; low bin edge: %s; '
                      'high bin edge: %s', x_nb, x_os, x_lo_bin, x_hi_bin)
            LGR.debug('y: no bins: %s; offset: %s; low bin edge: %s; '
                      'high bin edge: %s', y_nb, y_os, y_lo_bin, y_hi_bin)

            # Make a tree as data structure
            tree = defaultdict(lambda: defaultdict(float))
            # Fill tree
            for idx2 in range(0, len(dco.get_susy())):
                tree[dco.get_susy()[idx2]][dco.get_sm()[idx2]] += \
                dco.get_br()[idx2]

            # Define persistent counter
            counter = 0

            # Loop over the tree and if the total of the z values is less than
            # 100 %, multiply all numbers by a factor,
            # the difference comes from rounding
            zval_total = 0
            for ps_susy in range(0, dco.get_susy_ps()+1):
                for ps_sm in range(0, dco.get_sm_ps()+1):
                    zval_total += round(100*tree[ps_susy][ps_sm])
            if zval_total == 0:
                continue
            while zval_total < 100:
                zval_total = 0
                for ps_susy in range(0, dco.get_susy_ps()+1):
                    for ps_sm in range(0, dco.get_sm_ps()+1):
                        tree[ps_susy][ps_sm] *= 1.005
                        zval_total += round(100*tree[ps_susy][ps_sm])
            del zval_total

            for ps_susy in range(0, dco.get_susy_ps()+1):
                for ps_sm in range(0, dco.get_sm_ps()+1):

                    # Round z value to percentage
                    zval = round(100*tree[ps_susy][ps_sm])

                    LGR.debug('Subhisto fill: SUSY process: %s; SM '
                              'process: %s; fill: %s', ps_susy, ps_sm, zval)

                    # If 0 %, skip
                    if zval == 0.:
                        continue

                    # Unique name for sub TH2's
                    name = 'h{0:04d}{1:02d}{2:02d}'.format(idx1, ps_susy, ps_sm)
                    title = dco.get_ps(ps_susy, ps_sm)

                    # Create sub TH2's
                    subhisto = TH2F(name, title, 10, x_lo_bin, x_hi_bin,
                                    10, y_lo_bin, y_hi_bin)
                    color = dco.get_color(ps_susy, ps_sm)
                    subhisto.SetFillColor(color)

                    for _ in range(0, int(zval)):
                        c_x = float(x_lo_bin + (x_hi_bin-x_lo_bin)/
                                    10*(counter % 10) + (x_hi_bin-x_lo_bin)/20)
                        c_y = float(y_lo_bin + (y_hi_bin-y_lo_bin)/
                                    10*int(counter/10) + (y_hi_bin-y_lo_bin)/20)
                        LGR.debug('Subhisto fill: %s; %s', c_x, c_y)
                        # The z-value of 100 is empirical
                        subhisto.Fill(c_x, c_y, 100.)

                        counter += 1

                    self._dc_subhistos.append(subhisto)

                    # Check if process is already in list, otherwise add it
                    for histo in subhistos_legend:
                        # Only check for the last four digits (SUSY and SM
                        # process)
                        if histo.GetName()[-4:] == name[-4:]:
                            break
                    # If no break occured, add it to the list
                    else:
                        subhistos_legend.append(subhisto)

        # Draw all histograms
        self._histogram.Draw('BOX')
        for histo in self._dc_subhistos:
            histo.Draw('BOX SAME')

        self._add_legend(subhistos_legend)

    def _add_legend(self, histos):

        """ Add TLegend. """

        self._legend.SetBorderSize(0)
        #self._legend.SetTextSize(0.)
        for histo in histos:
            self._legend.AddEntry(histo, histo.GetTitle(), 'f')
        self._legend.Draw()

    def save(self):

        """ Save histogram in TFile and as *.pdf. """

        # Go into directory if it is defined
        if self.directory:
            cwd = getcwd()

            # Change directory in rootfile
            if not self.rootfile.GetDirectory(self.directory):
                self.rootfile.mkdir(self.directory)
            self.rootfile.cd(self.directory)

            # Change directory on filesystem
            if not path.exists(self.directory):
                makedirs(self.directory)
            chdir(self.directory)

        self._canvas.SaveAs('{}.pdf'.format(self._histogram.GetName()))
        self._histogram.Write()

        # Go back to original working directories
        if self.directory:
            self.rootfile.cd()
            chdir(cwd)

        self._clear()

    def plot_star(self, coordinates):

        """ Plot star at coordinates (x/y). """

        self._star = TGraph(1, array('d', [coordinates[0]]),
                            array('d', [coordinates[1]]))
        self._star.SetMarkerStyle(29)
        self._star.SetMarkerSize(2)
        self._canvas.cd()
        self._star.Draw('SAMEP')

    def plot_text(self, coordinates, l_text):

        """ Plot text at coordinates (x/y). """

        for idx, text in enumerate(l_text):
            latex = TLatex(coordinates[0], coordinates[1]-idx/15., text)
            latex.SetNDC()
            latex.SetTextColor(1)
            latex.SetTextFont(43)
            latex.SetTextSize(18)
            self._canvas.cd()
            latex.Draw()
            self._text.append(latex)

    def plot_diagonal(self):

        """ Plot diagonal line. """

        start = max(self._histogram.GetXaxis().GetXmin(),
                    self._histogram.GetYaxis().GetXmin())
        end = min(self._histogram.GetXaxis().GetXmax(),
                  self._histogram.GetYaxis().GetXmax())
        self.plot_line(start, start, end, end)

    def plot_line(self, x_1, y_1, x_2, y_2):

        """ Plot TLine. """

        self._line = TLine(x_1, y_1, x_2, y_2)
        self._line.SetLineColor(12)
        self._line.SetLineStyle(7)
        self._line.Draw("SAME")
