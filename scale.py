#!/usr/bin/env python2

""" Scale one TBranch in a TTree by a constant factor. """

from ToolboxTree import ToolboxTree

if __name__ == '__main__':
    MY_TOOLBOX = ToolboxTree()
    MY_TOOLBOX.name_rootfile_out = "singlelepton-trees/evVarFriend_WJetsToLNu_HT800to1200_scaled.root"
    MY_TOOLBOX.scale_branch("singlelepton-trees/evVarFriend_WJetsToLNu_HT800to1200_renamed.root", "WJETS_CENTRAL", 'Xsec', 1./1574633.0)
