#!/usr/bin/env python2

""" Plots the test statistic q_mu and the Asimov test statistic q_A ignoring
any effects from nuisance parameters. """

from math import factorial, exp, log, sqrt
from ROOT import TMath, Math  # pylint: disable=import-error

class TestStatistic(object):

    """ Class to plot test statistic q_mu and the Asimov test statistic q_A.
    """

    _mu = 0.
    _b = 0.  # pylint: disable=invalid-name
    _s = 0.  # pylint: disable=invalid-name
    _n = 0.  # pylint: disable=invalid-name

    def _get_mle(self):

        """ Return Maximum Likelihood Estimator. """

        try:
            return (self._n-self._b)/self._s
        except ZeroDivisionError:
            raise RuntimeError('Signal set to 0. Cannot compute MLE.')

    def _quantile(self, p_value):  # pylint: disable=no-self-use

        """ Return quantile of normal distribution for given p-value
        (p_value --> z_significance). """

        return TMath.NormQuantile(p_value)

    def _cdf(self, z_significance):  # pylint: disable=no-self-use

        """ Return upper tail of cumulative distribution function of given
        Z significance (z_significance --> p_value). """

        return Math.normal_cdf_c(z_significance)

    def likelihood_conditional(self):

        """ Return conditional likelihood. """

        return pow((self._mu*self._s + self._b), self._n) / \
                factorial(self._n) * exp(-(self._mu*self._s + self._b))

    def likelihood_unconditional(self):

        """ Return unconditional likelihood. """

        return pow((self._get_mle()*self._s + self._b), self._n) / \
                factorial(self._n) * exp(-(self._get_mle()*self._s + self._b))

    def test_statistic(self):

        """ Return test statistic. """

        return -2*log(self.likelihood_conditional() / \
                self.likelihood_unconditional())

    def test_statistic_asimov(self):

        """ Return Asimov test statistic. """

        # Store original value of n
        n_original = self._n
        # Overwrite n with b (Asimov!)
        self._n = self._b  # pylint: disable=invalid-name
        # Get value of test statistic
        q_a = self.test_statistic()
        # Restore value of n
        self._n = n_original
        # Return Asimov test statistic
        return q_a

    def obs_clsb(self):

        """ Return observed CLs+b = CDF(sqrt(q)). """

        return self._cdf(sqrt(self.test_statistic()))

    def obs_clb(self):

        """ Return observed CLb = CDF(sqrt(q) - sqrt(q_A)). """

        return self._cdf(sqrt(self.test_statistic())
                         -sqrt(self.test_statistic_asimov()))

    def obs_cls(self):

        """ Return observed CLs = CLs+b/CLb. """

        return self.obs_clsb()/self.obs_clb()

    def exp_clsb(self):

        """ Return expected CLs+b = CDF(sqrt(q_A)). """

        return self._cdf(sqrt(self.test_statistic_asimov()))

    def exp_clb(self):  # pylint: disable=no-self-use

        """ Return expected CLb = 0.5 (?). """

        return .5

    def exp_cls(self):

        """ Return expected CLs = CLs+b/CLb. """

        return self.exp_clsb()/self.exp_clb()

    # Public setter methods
    def set_mu_b_s_n(self, mu, b, s, n):  # pylint: disable=invalid-name
        """ Set values of mu, b, s and n. """
        self.set_mu(mu)
        self.set_b(b)
        self.set_s(s)
        self.set_n(n)

    def set_mu(self, val):
        """ Set value of mu. """
        self._mu = float(val)

    def set_b(self, val):
        """ Set value of b. """
        self._b = float(val)  # pylint: disable=invalid-name

    def set_s(self, val):
        """ Set value of s. """
        self._s = float(val)  # pylint: disable=invalid-name

    def set_n(self, val):
        """ Set value of n. """
        self._n = float(val)  # pylint: disable=invalid-name

    # Public getter methods
    def get_mu(self):
        """ Get value of mu. """
        return self._mu

    def get_b(self):
        """ Get value of b. """
        return self._b

    def get_s(self):
        """ Get value of s. """
        return self._s

    def get_n(self):
        """ Get value of n. """
        return self._n


if __name__ == "__main__":
    Q = TestStatistic()
    Q.set_mu_b_s_n(1., 2., 3., 5.)
    MUS = [1., 3.226556, 2.590658]
    for mu in MUS:
        Q.set_mu(mu)
        print 'mu set to {}.'.format(Q.get_mu())
        print 'unconditional likelihood'
        print Q.likelihood_unconditional()
        print 'conditional likelihood'
        print Q.likelihood_conditional()
        print 'test statistic'
        print Q.test_statistic()
        print 'asimov test statistic'
        print Q.test_statistic_asimov()
        print 'Observed CLsb'
        print Q.obs_clsb()
        print 'Observed CLb'
        print Q.obs_clb()
        print 'Observed CLs'
        print Q.obs_cls()
        print 'Expected CLsb'
        print Q.exp_clsb()
        print 'Expected CLb ???'
        print Q.exp_clb()
        print 'Expected CLs'
        print Q.exp_cls()
