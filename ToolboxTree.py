#!/usr/bin/env python2

""" Toolbox classes for various operations on ROOT TTrees. """

from array import array
from ROOT import TFile, TTree, TObject, TChain  # pylint: disable=import-error
from Logger import LGR
from ToolboxHelper import check_if_list, check_if_file_exists, \
                          check_if_object, check_if_tree_exists


class ToolboxTree(object):

    """ Toolbox class for various operations on ROOT TTrees. """

    def __init__(self):
        # Boolean to check if name_rootfile_out has been set by user
        # If not, throw warning
        self._name_rootfile_out_changed = False
        # Name of output rootfile
        self._name_rootfile_out = 'output.root'
        # Branches to be selected
        self._branches = ['*']

    @property
    def name_rootfile_out(self):

        """ Hide the method name_rootfile_out() as attribute; this is done to
        detect user changes. """

        return self._name_rootfile_out

    @name_rootfile_out.setter
    def name_rootfile_out(self, value):

        """ Setter for name_rootfile_out() where we also set a boolean to True
        to later check if this value has been set by the user or not
        (otherwise, throw a warning that the default value has been used). """

        self._name_rootfile_out_changed = True
        self._name_rootfile_out = value

    def set_branches(self, branches):

        """ Set branches to be selected when copying to new TTree. """

        LGR.info('Select only following branches: %s', branches)
        self._branches = branches

    def merge_trees_multiple(self, list_trees_merge_multiple):

        """ Merge multiple TTrees from possibly multiple TFiles; this does no
        ROOT operations itself, but calls merge_trees(); call as:
        merge_trees_multiple([
            ['outputtree1', ['in1.root', 'tree'], ['in2.root', 'tree']],
            ['outputtree2', ['in1.root', 'tree']],
            ...
        ]). """

        LGR.debug('In function %s()', self.merge_trees_multiple.__name__)
        check_if_list(list_trees_merge_multiple)

        LGR.debug('name_rootfile_out: %s', self.name_rootfile_out)
        for list_trees_merge in list_trees_merge_multiple:
            check_if_list(list_trees_merge)
            name_roottree_out = list_trees_merge[0]
            LGR.debug('name_roottree_out: %s', name_roottree_out)
            self.merge_trees(list_trees_merge[0], list_trees_merge[1:])

    def merge_trees(self, name_roottree_out, list_trees_merge):

        """ Merge TTrees from possibly multiple TFiles; call as:
        merge_trees('outputtree',
            [['in1.root', 'tree'], ['in1.root', 'tree']], ...). """

        LGR.debug('In function %s()', self.merge_trees.__name__)
        check_if_list(list_trees_merge, 1)

        # If the output ROOT filename has not been set by the user, throw a
        # warning
        if not self._name_rootfile_out_changed:
            LGR.warning('Output ROOT file name has not been set. Using '
                        'default: %s', self.name_rootfile_out)

        LGR.info('Trees to be merged as %s in %s: %s',
                 name_roottree_out, self.name_rootfile_out, list_trees_merge)
        for trees_merge_n, trees_merge in enumerate(list_trees_merge):

            check_if_list(trees_merge, 2, 2)
            name_rootfile_in = trees_merge[0]
            name_roottree_in = trees_merge[1]
            LGR.debug('name_rootfile_in: %s', name_rootfile_in)
            LGR.debug('name_roottree_in: %s', name_roottree_in)

            check_if_file_exists(name_rootfile_in)
            check_if_tree_exists(name_rootfile_in, name_roottree_in)

            # Set up TChain
            if trees_merge_n == 0:
                chain_roottree = TChain(name_roottree_in)
                chain_roottree.AddFile(name_rootfile_in)
            else:
                chain_roottree.AddFile('{}/{}'
                                       .format(name_rootfile_in,
                                               name_roottree_in))

            # Select branches to be copied to the new tree
            for tree in chain_roottree:
                tree.SetBranchStatus("*", 0)
                for branch in self._branches:
                    tree.SetBranchStatus(branch, 1)

        # Need to open output TFile here, for the subsequently created TTree to
        # not be memory-resident
        LGR.debug('Open output root file %s.', self.name_rootfile_out)
        rootfile_out = TFile(self.name_rootfile_out, 'UPDATE')

        # Merge TTrees in TChain, this creates the merged TTree in memory
        roottree_out = chain_roottree.CloneTree(-1, 'fast')
        roottree_out.SetNameTitle(name_roottree_out, name_roottree_out)
        # Write the TTree to the TFile and overwrite a possible duplicate
        roottree_out.Write(name_roottree_out, TObject.kOverwrite)

        # Close all the TFiles
        rootfile_out.Close()

        LGR.debug('%s in %s written and root file closed.',
                  name_roottree_out, self.name_rootfile_out)
        del rootfile_out, chain_roottree, roottree_out

    def compare_branches_str(self, file1, tree1, file2, tree2):

        """ Compare TBranches of the two TTrees tree1 and tree2 from TFiles
        file 1 and file 2; all arguments are given as strings. """

        LGR.debug('In function %s().', self.compare_branches_str.__name__)

        check_if_file_exists(file1)
        check_if_file_exists(file2)
        check_if_tree_exists(file1, tree1)
        check_if_tree_exists(file2, tree2)
        rootfile1 = TFile(file1, 'READ')
        rootfile2 = TFile(file2, 'READ')
        roottree1 = rootfile1.Get(tree1)
        roottree2 = rootfile2.Get(tree2)

        self.compare_branches_tree(roottree1, roottree2)

    def compare_branches_tree(self, roottree1, roottree2):

        """ Compare TBranches of the two TTrees roottree1 and roottree2. """

        LGR.debug('In function %s().', self.compare_branches_tree.__name__)

        check_if_object(roottree1, TTree)
        check_if_object(roottree2, TTree)

        rootbranches1 = roottree1.GetListOfBranches()
        rootbranches2 = roottree2.GetListOfBranches()

        branches1 = []
        branches2 = []
        for rootbranch1 in rootbranches1:
            branches1.append(rootbranch1.GetName())
        for rootbranch2 in rootbranches2:
            branches2.append(rootbranch2.GetName())

        self.compare_lists(branches1, branches2)

    def compare_lists(self, list1, list2):

        """ Compare content of two lists. """

        LGR.debug('In function %s().', self.compare_lists.__name__)

        print 'Common TBranches: {}'.format(set(list1).intersection(list2))
        print 'TBranches only in {}: {}'.format('first list', set(list1)
                                                .difference(list2))
        print 'TBranches only in {}: {}'.format('second list', set(list2)
                                                .difference(list1))

    def scale_branch(self, path_file, path_tree, branch, weight):

        """ Scale TBranch in TTree in TFile by weight. """

        check_if_file_exists(path_file)
        check_if_tree_exists(path_file, path_tree)
        rootfile_in = TFile(path_file, 'READ')
        roottree_in = rootfile_in.Get(path_tree)

        LGR.info('Scale TBranch %s in TTree %s in TFile %s by %s.',
                 branch, path_tree, path_file, weight)

        rootfile_out = TFile(self._name_rootfile_out, 'UPDATE')
        roottree_out = roottree_in.CloneTree(0)

        # Check if the type is float, this will not catch doubles
        if not isinstance(roottree_in.GetLeaf(branch).GetValue(), float):
            raise TypeError('The branch you specified is not of type float. '
                            'This is currently not implemented.')

        LGR.warning('I assume your branch is of type float. If it is not (e.g.'
                    ' it\'s a double), the output will be undefined. Check you'
                    'r output carefully!')

        # ROOT needs a C style pointer to use SetBranchAddress()
        arr_br = array('f', [0.])
        roottree_out.SetBranchAddress(branch, arr_br)

        for event in roottree_in:
            arr_br[0] = getattr(event, branch)*weight
            roottree_out.Fill()
        roottree_out.Write()
        rootfile_out.Close()

    def rename_branch(self, path_file, path_tree, branch_old, branch_new):

        """ Rename TBranch in TTree in TFile. """

        check_if_file_exists(path_file)
        check_if_tree_exists(path_file, path_tree)
        rootfile_in = TFile(path_file, 'READ')
        roottree_in = rootfile_in.Get(path_tree)

        LGR.info('Rename TBranch %s in TTree %s in TFile %s to %s.',
                 branch_old, path_tree, path_file, branch_new)
        LGR.warning('For now, this method adds a new TBranch, but the old one '
                    'will still be available.')
        LGR.warning('I assume your branch is of type float. If it is not (e.g.'
                    ' it\'s a double), the output will be undefined. Check you'
                    'r output carefully!')

        # Deactivate old TBranch
        #roottree_in.SetBranchStatus(branch_old, 0)

        rootfile_out = TFile(self._name_rootfile_out, 'UPDATE')
        roottree_out = roottree_in.CloneTree(0)

        # ROOT needs a C style pointer to use Branch()
        arr_br = array('f', [5.])
        branch = roottree_out.Branch(branch_new, arr_br,
                                     '{}/F'.format(branch_new))

        for event in roottree_in:
            arr_br[0] = getattr(event, branch_old)
            branch.Fill()
            roottree_out.Fill()
        roottree_out.Write()
        rootfile_out.Close()
