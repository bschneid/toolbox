#!/usr/bin/env python2

""" Merges multiple TTrees from possibly different TFiles into one TTree """

from ToolboxTree import ToolboxTree

if __name__ == "__main__":
    MY_TOOLBOX = ToolboxTree()

    ROOTFILES = [
        'singlelepton-trees/evVarFriend_DYJetsToLL_M50_HT100to200',
        'singlelepton-trees/evVarFriend_DYJetsToLL_M50_HT200to400',
        'singlelepton-trees/evVarFriend_DYJetsToLL_M50_HT400to600',
        'singlelepton-trees/evVarFriend_DYJetsToLL_M50_HT600toInf',
        'singlelepton-trees/evVarFriend_QCD_HT1000to1500',
        'singlelepton-trees/evVarFriend_QCD_HT1500to2000',
        'singlelepton-trees/evVarFriend_QCD_HT2000toInf',
        'singlelepton-trees/evVarFriend_QCD_HT300to500',
        'singlelepton-trees/evVarFriend_QCD_HT500to700',
        'singlelepton-trees/evVarFriend_QCD_HT700to1000',
        'singlelepton-trees/evVarFriend_T1tttt_mGo1200_mChi800',
        'singlelepton-trees/evVarFriend_TBar_tWch',
        'singlelepton-trees/evVarFriend_TTJets_DiLepton',
        'singlelepton-trees/evVarFriend_TTJets_LO',
        'singlelepton-trees/evVarFriend_TTJets_LO_HT1200to2500',
        'singlelepton-trees/evVarFriend_TTJets_LO_HT2500toInf',
        'singlelepton-trees/evVarFriend_TTJets_LO_HT600to800',
        'singlelepton-trees/evVarFriend_TTJets_LO_HT800to1200',
        'singlelepton-trees/evVarFriend_TTJets_SingleLeptonFromT',
        'singlelepton-trees/evVarFriend_TTJets_SingleLeptonFromTbar',
        'singlelepton-trees/evVarFriend_TTWToLNu',
        'singlelepton-trees/evVarFriend_TTWToQQ',
        'singlelepton-trees/evVarFriend_TTZToLLNuNu',
        'singlelepton-trees/evVarFriend_TTZToQQ',
        'singlelepton-trees/evVarFriend_TToLeptons_sch',
        'singlelepton-trees/evVarFriend_TToLeptons_tch_amcatnlo_full',
        'singlelepton-trees/evVarFriend_T_tWch',
        'singlelepton-trees/evVarFriend_WJetsToLNu_HT100to200',
        'singlelepton-trees/evVarFriend_WJetsToLNu_HT1200to2500',
        'singlelepton-trees/evVarFriend_WJetsToLNu_HT200to400',
        'singlelepton-trees/evVarFriend_WJetsToLNu_HT2500toInf',
        'singlelepton-trees/evVarFriend_WJetsToLNu_HT400to600',
        'singlelepton-trees/evVarFriend_WJetsToLNu_HT600to800',
        'singlelepton-trees/evVarFriend_WJetsToLNu_HT800to1200',
    ]
    PROCESSES = [
        'DY_CENTRAL',
        'DY_CENTRAL',
        'DY_CENTRAL',
        'DY_CENTRAL',
        'QCD_CENTRAL',
        'QCD_CENTRAL',
        'QCD_CENTRAL',
        'QCD_CENTRAL',
        'QCD_CENTRAL',
        'QCD_CENTRAL',
        'T1tttt_mGo1200_mChi800_CENTRAL',
        'SINGLETOP_CENTRAL',
        'TTBAR_CENTRAL',
        'TTBAR_CENTRAL',
        'TTBAR_CENTRAL',
        'TTBAR_CENTRAL',
        'TTBAR_CENTRAL',
        'TTBAR_CENTRAL',
        'TTBAR_CENTRAL',
        'TTBAR_CENTRAL',
        'TTV_CENTRAL',
        'TTV_CENTRAL',
        'TTV_CENTRAL',
        'TTV_CENTRAL',
        'SINGLETOP_CENTRAL',
        'SINGLETOP_CENTRAL',
        'SINGLETOP_CENTRAL',
        'WJETS_CENTRAL',
        'WJETS_CENTRAL',
        'WJETS_CENTRAL',
        'WJETS_CENTRAL',
        'WJETS_CENTRAL',
        'WJETS_CENTRAL',
        'WJETS_CENTRAL'
    ]

    for i, rootfile in enumerate(ROOTFILES):
        process = PROCESSES[i]
        MY_TOOLBOX.name_rootfile_out = '{}_renamed.root'.format(rootfile)
        MY_TOOLBOX.merge_trees(process, [['{}.root'.format(rootfile), 'sf/t']])


    #MY_TOOLBOX.merge_trees('testOFaNAME', [['in1.root',
    #                       'tree'], ['in2.root', 'tree']])
    # MY_TOOLBOX.merge_trees('tree1', [['DYJetsToLL_M50_HT100to200.root',
    #     'tree']])
    # MY_TOOLBOX.name_rootfile_out = 'in2.root'
    # MY_TOOLBOX.merge_trees('tree2', [['DYJetsToLL_M50_HT200to400.root',
    #     'tree']])
    # MY_TOOLBOX.merge_trees('dy1dy1',
    #         [['in1.root', 'tree'], ['in1.root', 'tree']])
    # MY_TOOLBOX.merge_trees('dy1dy2', [['in1.root', 'tree'], ['in2.root',
    #     'tree']])
    # MY_TOOLBOX.compare_branches_str('in1.root', 'tree', 'in2.root', 'tree')
